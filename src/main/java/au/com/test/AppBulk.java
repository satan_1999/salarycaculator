package au.com.test;

import java.util.List;

import au.com.test.domain.Employee;
import au.com.test.inputs.EmployeesFileLoader;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import au.com.test.domain.Salary;
import au.com.test.service.SalaryService;


public class AppBulk {
    private static Logger log = Logger.getLogger(AppBulk.class);

    /**
     * Start to run the program
     *
     * @param args
     */
    public static void main(final String[] args) {
        BasicConfigurator.configure();
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        log.setLevel(Level.DEBUG);
        SalaryService salaryService = (SalaryService) context.getBean("salaryService");
        // load employees from file, init() can be called when spring creates the bean
        EmployeesFileLoader salaryBulk = (EmployeesFileLoader) context.getBean("employeesFileLoader");
        List<Employee> employees = salaryBulk.readEmployeeFromFile(salaryBulk.generateFilePath(args));
        // calculate salary for each employee
        List<Salary> salaries = salaryService.calculateSalaries(employees);
        // print out the salary
        for (Salary salary : salaries) {
            System.out.println(salary.toString());
        }
    }
}
