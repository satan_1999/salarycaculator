package au.com.test.handlers;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;
import au.com.test.service.SalaryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SalaryRootHandler {
    private static Logger log = Logger.getLogger(SalaryService.class);

    private SalaryCalculationHandler firstHandler;

    @Autowired
    private SalaryGrossIncomeHandler salaryGrossIncomeHandler;
    @Autowired
    private SalaryPayPeriodHandler salaryPayPeriodHandler;
    @Autowired
    private SalaryNameHandler salaryNameHandler;
    @Autowired
    private SalarySuperHandler salarySuperHandler;
    @Autowired
    private SalaryIncomeTaxHandler salaryIncomeTaxHandler;

    @PostConstruct
    public void init() {
        log.info("Initializing SalaryRootHandler");
        firstHandler = salaryNameHandler;
        firstHandler.addNext(salaryGrossIncomeHandler)
                .addNext(salaryIncomeTaxHandler)
                .addNext(salarySuperHandler)
                .addNext(salaryPayPeriodHandler);
    }

    public Salary.SalaryBuilder calculate(Employee employee) {
        return firstHandler.calculate(null, employee);
    }
}
