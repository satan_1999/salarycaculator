package au.com.test.handlers;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;

public interface SalaryCalculationHandler {

    Salary.SalaryBuilder calculate(Salary.SalaryBuilder salaryBuilder , Employee employee);

    SalaryCalculationHandler addNext(SalaryCalculationHandler next);

}
