package au.com.test.handlers;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;
import au.com.test.service.TaxChartHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SalaryIncomeTaxHandler extends AbstractSalaryCalculationHandler {

    @Autowired
    private TaxChartHelper taxChartHelper;

    @Override
    public Salary.SalaryBuilder calculate(Salary.SalaryBuilder salaryBuilder, Employee employee) {
        salaryBuilder.setIncomeTax(taxChartHelper.getTaxAmount(employee.getPayYear(), employee) / 12);
        return next() == null ? salaryBuilder : next().calculate(salaryBuilder, employee);
    }

}
