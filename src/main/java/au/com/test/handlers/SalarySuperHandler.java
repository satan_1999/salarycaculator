package au.com.test.handlers;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;
import org.springframework.stereotype.Component;

@Component
public class SalarySuperHandler extends AbstractSalaryCalculationHandler {

    @Override
    public Salary.SalaryBuilder calculate(Salary.SalaryBuilder salaryBuilder, Employee employee) {
        salaryBuilder.setSuperannuation(salaryBuilder.getGrossIncome().doubleValue() * employee.getSuperRate());
        return next() == null ? salaryBuilder : next().calculate(salaryBuilder, employee);
    }

}
