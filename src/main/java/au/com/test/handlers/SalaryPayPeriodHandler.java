package au.com.test.handlers;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;
import org.springframework.stereotype.Component;


@Component
public class SalaryPayPeriodHandler extends AbstractSalaryCalculationHandler {

    @Override
    public Salary.SalaryBuilder calculate(Salary.SalaryBuilder salaryBuilder, Employee employee) {
        salaryBuilder.setPayPeriod(employee.getPaymentPeriod());
        return next() == null ? salaryBuilder : next().calculate(salaryBuilder, employee);
    }
}
