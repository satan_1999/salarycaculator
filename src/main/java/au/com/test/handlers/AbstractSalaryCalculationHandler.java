package au.com.test.handlers;


abstract class AbstractSalaryCalculationHandler implements SalaryCalculationHandler {

    private SalaryCalculationHandler nextHandler;

    public SalaryCalculationHandler next() {
        return nextHandler;
    }

    public SalaryCalculationHandler addNext(SalaryCalculationHandler nextHandler) {
        this.nextHandler = nextHandler;
        return nextHandler;
    }

}
