package au.com.test.handlers;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;
import org.springframework.stereotype.Component;

@Component
public class SalaryGrossIncomeHandler extends AbstractSalaryCalculationHandler {

    @Override
    public Salary.SalaryBuilder calculate(Salary.SalaryBuilder salaryBuilder, Employee employee) {
        salaryBuilder.setGrossIncome(employee.getAnnualSalary().doubleValue()/12);
        return next() == null ? salaryBuilder : next().calculate(salaryBuilder, employee);
    }
}
