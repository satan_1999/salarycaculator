package au.com.test.handlers;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;
import org.springframework.stereotype.Component;

@Component
public class SalaryNameHandler extends AbstractSalaryCalculationHandler {

    @Override
    public Salary.SalaryBuilder calculate(Salary.SalaryBuilder salaryBuilder, Employee employee) {

        if (salaryBuilder == null) {
            salaryBuilder = new Salary.SalaryBuilder();
        }
        salaryBuilder.setName(employee.getFirstName() + " " + employee.getLastName());

        return next() == null ? salaryBuilder : next().calculate(salaryBuilder, employee);
    }
}
