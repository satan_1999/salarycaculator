package au.com.test.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import au.com.test.handlers.SalaryRootHandler;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;

@Service
public class SalaryService {

    private static Logger log = Logger.getLogger(SalaryService.class);

    @Autowired
    private SalaryRootHandler salaryRootHandler;

    /**
     * calculate salary for all employee
     */
    public List<Salary> calculateSalaries(List<Employee> employees) {
        if (employees == null || employees.isEmpty()) {
            return new ArrayList<>();
        }
        return employees.stream().map(this::calculateSalary).collect(Collectors.toList());
    }

    public Salary calculateSalary(Employee employee) {
        // calculate salary for each employee
        return salaryRootHandler.calculate(employee).build();
    }


}
