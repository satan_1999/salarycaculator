package au.com.test.service;

import au.com.test.domain.Employee;
import au.com.test.tax.TaxChart;
import au.com.test.tax.TaxChartHelper2012Imp;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.HashMap;


@Service
public class TaxChartHelper {
    private HashMap<Integer, TaxChart> yearlyTaxCharts = new HashMap<>();

    @PostConstruct
    private void initReader() {
        register(2012, new TaxChartHelper2012Imp());
        register(2013, new TaxChartHelper2012Imp());
        register(2014, new TaxChartHelper2012Imp());
        register(2015, new TaxChartHelper2012Imp());
        register(2016, new TaxChartHelper2012Imp());
        register(2017, new TaxChartHelper2012Imp());
        register(2018, new TaxChartHelper2012Imp());
    }

    public void register(Integer year, TaxChart taxChart) {
        yearlyTaxCharts.put(year, taxChart);
    }

    public double getTaxAmount(Integer year, Employee employee) {
        TaxChart taxChart = yearlyTaxCharts.get(year);
        Assert.notNull(taxChart, "There is no taxChart for Year - " + year);
        return taxChart.getTaxRate(employee);
    }
}
