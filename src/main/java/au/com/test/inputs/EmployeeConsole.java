package au.com.test.inputs;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;
import au.com.test.service.SalaryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
public class EmployeeConsole {
    private static final String CMD_EXIT = "EXIT";
    private static Logger log = Logger.getLogger(EmployeeConsole.class);
    private BufferedReader reader;

    @Autowired
    private SalaryService salaryService;

    @PostConstruct
    private void initReader() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    @PreDestroy
    private void destroyReader() {
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void startConsole() {
        printBanner();

        while (true) {
            log.info("Please Type in Pay Details in comma delimited form(FirstName,LastName,AnnualSalary,SuperPercentage,salaryMonthAndYear) to get Pay Slip or Type " + CMD_EXIT + " to close application.");
            String consoleInput = readLine();

            if (consoleInput.equalsIgnoreCase(CMD_EXIT)) {
                log.info("Game Over");
                break;
            }
            log.info(generateSalary(consoleInput));
        }
    }

    private void printBanner() {
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("++++++++++++++ Salary Calculator ++++++++++++++");
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++");
    }

    private String readLine() {
        String consoleInput = "";
        try {
            consoleInput = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return consoleInput;
    }

    private String generateSalary(String consoleInput) {
        String result;
        try {
            Salary salary = salaryService.calculateSalary(new Employee.EmployeeBuilder().buildEmployeeWithLine(consoleInput).build());
            result = salary.toString();
        } catch (Exception e) {
            result = e.getMessage();
        }
        return result;
    }
}
