package au.com.test.inputs;

import au.com.test.domain.Employee;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class EmployeesFileLoader {

    private static Logger log = Logger.getLogger(EmployeesFileLoader.class);
    private static final String DEFAULT_DATES_DAT = "/data.csv";

    public String generateFilePath(String[] args) {
        String datafileLocation;
        if (args == null || args.length == 0) {
            datafileLocation = EmployeesFileLoader.class.getClassLoader().getResource(".").getPath() + DEFAULT_DATES_DAT;
        } else {
            System.out.println("Found args -- " + args[0]);
            datafileLocation = args[0];
        }
        return datafileLocation;
    }

    /**
     * Read Employee information from file and create Employee domain class
     *
     * @param fileName
     * @return
     * @throws IOException
     */
    public List<Employee> readEmployeeFromFile(String fileName) {
        Assert.notNull(fileName, "The fileName must not be null");
        List<Employee> employees = new ArrayList<>();
        BufferedReader in = null;

        try {
            File file = new File(fileName);
            in = new BufferedReader(new FileReader(file));

            String line;
            while ((line = in.readLine()) != null) {
                // populate Employee domain
                employees.add(new Employee.EmployeeBuilder().buildEmployeeWithLine(line).build());
            }
            log.info("Loaded Employees  " + employees.size());
            in.close();
        } catch (IOException e) {
            log.error("Failed to load file", e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.error("Failed to close file", e);
                }
            }
        }
        return employees;
    }
}
