package au.com.test.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.util.Assert;

public class Employee {
    private static Logger log = Logger.getLogger(Employee.class);
    private static final char LINE_SEPARATOR = ',';
    private static final String PERCENTAGE_SIGN = "%";
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

    private String firstName;
    private String lastName;
    private BigDecimal annualSalary;
    private double superRate;
    private Date payStart;
    private Date payEnd;

    public int getPayYear() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(payStart);
        return cal.get(Calendar.YEAR);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public BigDecimal getAnnualSalary() {
        return annualSalary;
    }

    public double getSuperRate() {
        return superRate;
    }

    public String getPaymentPeriod() {
        return String.format("%s - %s", dateFormat.format(payStart), dateFormat.format(payEnd));
    }


    public static class EmployeeBuilder {

        public static final String MONTH_YEAR_SEPARATOR = "/";
        private String firstName;
        private String lastName;
        private BigDecimal annualSalary;
        private double superRate;
        private Date payStart;
        private Date payEnd;

        public Employee build() {
            Employee employee = new Employee();
            employee.firstName = this.firstName;
            employee.lastName = this.lastName;
            employee.annualSalary = this.annualSalary;
            employee.superRate = this.superRate;
            employee.payStart = this.payStart;
            employee.payEnd = this.payEnd;
            return employee;
        }

        public EmployeeBuilder buildEmployeeWithLine(String line) {

            String[] splitLine = StringUtils.split(line, LINE_SEPARATOR);
            Assert.isTrue(splitLine.length == 5, "Invalid Input! Example of Valid Input: David,Rud,60050,9,3/2017");
            try {
                String[] payPeriodDetails = splitLine[4].split(MONTH_YEAR_SEPARATOR);
                LocalDate payFrom = new LocalDate(new Integer(payPeriodDetails[1]), new Integer(payPeriodDetails[0]), 1);
                LocalDate payTo = new LocalDate(new Integer(payPeriodDetails[1]), new Integer(payPeriodDetails[0]), 1).plusMonths(1).withDayOfMonth(1).minusDays(1);

                this.setFirstName(splitLine[0])
                        .setLastName(splitLine[1])
                        .setAnnualSalary(Integer.parseInt(splitLine[2]))
                        .setSuperRate(splitLine[3])
                        .setPayStart(payFrom.toDate())
                        .setPayEnd(payTo.toDate());

                return this;
            } catch (Exception ex) {
                log.error("Failed to setEmployee", ex);
                throw new IllegalArgumentException("This line is not correct");
            }
        }

        public EmployeeBuilder setPayStart(Date payStart) throws ParseException {
            this.payStart = payStart;
            return this;
        }

        public EmployeeBuilder setPayEnd(Date payEnd) throws ParseException {
            this.payEnd = payEnd;
            return this;
        }


        public EmployeeBuilder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public EmployeeBuilder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public EmployeeBuilder setAnnualSalary(int annualSalary) {
            this.annualSalary = new BigDecimal(annualSalary).setScale(0, RoundingMode.HALF_UP);
            return this;
        }


        public EmployeeBuilder setSuperRate(String superRateStr) {
            if (superRateStr != null && superRateStr.endsWith(PERCENTAGE_SIGN)) {
                String superNumber = superRateStr.replaceAll(PERCENTAGE_SIGN, "");
                this.superRate = Double.parseDouble(superNumber) / 100;
                Assert.isTrue((this.superRate > 0 && this.superRate <= 0.5));
            }
            return this;
        }
    }
}
