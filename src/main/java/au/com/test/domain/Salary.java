package au.com.test.domain;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Salary {

    private String name;
    private String payPeriod;
    private BigDecimal grossIncome;
    private BigDecimal incomeTax;
    private BigDecimal superannuation;

    public String getName() {
        return name;
    }

    private String getPayPeriod() {
        return payPeriod;
    }

    public BigDecimal getGrossIncome() {
        return grossIncome;
    }

    public BigDecimal getIncomeTax() {
        return incomeTax;
    }

    public BigDecimal getNetIncome() {
        return grossIncome.subtract(incomeTax, new MathContext(0));
    }

    public BigDecimal getSuperannuation() {
        return superannuation;
    }

    public String toString() {
        return this.getName() + "," + this.getPayPeriod() + ","
                + this.getGrossIncome() + "," + this.getIncomeTax() + ","
                + this.getNetIncome() + "," + this.getSuperannuation();
    }


    public static class SalaryBuilder {
        private String name;
        private String payPeriod;
        private BigDecimal grossIncome;
        private BigDecimal incomeTax;
        private BigDecimal superannuation;


        public Salary build() {
            Salary salary = new Salary();
            salary.name = this.name;
            salary.payPeriod = this.payPeriod;
            salary.grossIncome = this.grossIncome;
            salary.incomeTax = this.incomeTax;
            salary.superannuation = this.superannuation;
            return salary;
        }


        public SalaryBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public SalaryBuilder setPayPeriod(String payPeriod) {
            this.payPeriod = payPeriod;
            return this;
        }

        public SalaryBuilder setSuperannuation(double superannuation) {
            this.superannuation = new BigDecimal(superannuation).setScale(0,
                    RoundingMode.HALF_UP);
            return this;
        }

        public SalaryBuilder setIncomeTax(double tax) {
            this.incomeTax = new BigDecimal(tax).setScale(0,
                    RoundingMode.HALF_UP);
            return this;
        }

        public SalaryBuilder setGrossIncome(double grossIncome) {
            this.grossIncome = new BigDecimal(grossIncome).setScale(0,
                    RoundingMode.HALF_UP);
            return this;
        }

        public String getName() {
            return name;
        }

        public String getPayPeriod() {
            return payPeriod;
        }

        public BigDecimal getGrossIncome() {
            return grossIncome;
        }

        public BigDecimal getIncomeTax() {
            return incomeTax;
        }

        public BigDecimal getSuperannuation() {
            return superannuation;
        }

    }

}
