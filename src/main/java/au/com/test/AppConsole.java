package au.com.test;

import au.com.test.inputs.EmployeeConsole;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


class AppConsole {
    private static Logger log = Logger.getLogger(AppConsole.class);

    public static void main(final String[] args) {
        BasicConfigurator.configure();
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        log.setLevel(Level.INFO);
        EmployeeConsole console = context.getBean(EmployeeConsole.class);
        console.startConsole();
    }
}
