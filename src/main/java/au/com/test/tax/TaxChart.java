package au.com.test.tax;

import au.com.test.domain.Employee;
public interface TaxChart {

    double getTaxRate(Employee employee);
}
