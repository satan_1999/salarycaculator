package au.com.test.tax;

import au.com.test.domain.Employee;

public class TaxChartHelper2012Imp implements TaxChart {

    public double getTaxRate(Employee employee) {
        int annualSalary = employee.getAnnualSalary().intValue();
        double incomeTax = 0;
        if (0 <= annualSalary && annualSalary <= 18200) {
            incomeTax = 0;
        } else if (18201 <= annualSalary && annualSalary <= 37000) {
            incomeTax = (annualSalary - 18200) * 0.19;
        } else if (37001 <= annualSalary && annualSalary <= 80000) {
            incomeTax = (annualSalary - 37000) * 0.325 + 3572;
        } else if (80001 <= annualSalary && annualSalary <= 180000) {
            incomeTax = (annualSalary - 80000) * 0.37 + 17547;
        } else if (180001 <= annualSalary) {
            incomeTax = (annualSalary - 180000) * 0.45 + 54547;
        }
        return incomeTax ;

    }
}
