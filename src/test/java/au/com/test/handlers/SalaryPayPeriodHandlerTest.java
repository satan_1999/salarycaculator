package au.com.test.handlers;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;

public class SalaryPayPeriodHandlerTest {
    private SalaryPayPeriodHandler handler = new SalaryPayPeriodHandler();

    @Test
    public void testCalculate() throws Exception {
        Salary.SalaryBuilder sb  = new Salary.SalaryBuilder().setGrossIncome(60050);
        Employee em = new Employee.EmployeeBuilder().setPayStart(new LocalDate(2015, 3, 1).toDate()).setPayEnd(new LocalDate(2015, 3, 31).toDate()).build();
        handler.calculate(sb, em);
        Assert.assertEquals("01 March 2015 - 31 March 2015", sb.getPayPeriod());
    }

}