package au.com.test.handlers;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;
import org.junit.Assert;
import org.junit.Test;

public class SalarySuperHandlerTest {

    private SalarySuperHandler handler = new SalarySuperHandler();

    @Test
    public void testCalculate() throws Exception {
        Employee em = new Employee.EmployeeBuilder().buildEmployeeWithLine("David,Rudd,60050,9%,03/2013").build();
        Salary.SalaryBuilder sb  = new Salary.SalaryBuilder().setGrossIncome(60050 / 12);
        handler.calculate(sb, em);

        Assert.assertEquals(450, sb.getSuperannuation().intValue());
    }

}