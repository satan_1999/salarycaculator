package au.com.test.handlers;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;
import org.junit.Assert;
import org.junit.Test;

public class SalaryGrossIncomeHandlerTest {

    private SalaryGrossIncomeHandler handler = new SalaryGrossIncomeHandler();

    @Test
    public void testCalculate() throws Exception {
        Salary.SalaryBuilder sb = new Salary.SalaryBuilder().setGrossIncome(60050);
        Employee em = new Employee.EmployeeBuilder().buildEmployeeWithLine("T1,T2,60050,9%,03/2013").build();
        handler.calculate(sb, em);
        Assert.assertEquals(5004, sb.getGrossIncome().intValue());
    }
}