package au.com.test.handlers;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;
import au.com.test.service.TaxChartHelper;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class SalaryIncomeTaxHandlerTest {

    @Mock
    private TaxChartHelper taxChartHelper;

    @InjectMocks
    private SalaryIncomeTaxHandler handler ;

    @Test
    public void testCalculate() throws Exception {
        Salary.SalaryBuilder sb = new Salary.SalaryBuilder().setGrossIncome(60050);
        Employee em = new Employee.EmployeeBuilder().setPayStart(new LocalDate(2015, 3, 1).toDate()).setPayEnd(new LocalDate(2015, 3, 31).toDate()).build();

        when(taxChartHelper.getTaxAmount(any(), any())).thenReturn(111.11 * 12);

        handler.calculate(sb, em);
        Assert.assertEquals(111.11, sb.getIncomeTax().doubleValue(), 001);
    }
}