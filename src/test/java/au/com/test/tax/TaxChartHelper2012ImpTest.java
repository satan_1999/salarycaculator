package au.com.test.tax;

import au.com.test.domain.Employee;
import org.junit.Assert;
import org.junit.Test;

public class TaxChartHelper2012ImpTest {

    private TaxChartHelper2012Imp taxChart = new TaxChartHelper2012Imp();

    @Test
    public void testGetTaxRate_Below18200() throws Exception {

        Employee em = new Employee.EmployeeBuilder().buildEmployeeWithLine("David,Rudd,2050,9%,03/2013").build();
        Assert.assertEquals(0.0, taxChart.getTaxRate(em), 0.001);
    }

    @Test
    public void testGetTaxRate_Over18200() throws Exception {

        Employee em = new Employee.EmployeeBuilder().buildEmployeeWithLine("David,Rudd,20050,9%,03/2013").build();
        Assert.assertEquals(351.5, taxChart.getTaxRate(em), 0.001);
    }

    @Test
    public void testGetTaxRate_Over37000() throws Exception {

        Employee em = new Employee.EmployeeBuilder().buildEmployeeWithLine("David,Rudd,60050,9%,03/2013").build();
        Assert.assertEquals(11063.25, taxChart.getTaxRate(em), 0.001);
    }

    @Test
    public void testGetTaxRate_Over80000() throws Exception {

        Employee em = new Employee.EmployeeBuilder().buildEmployeeWithLine("David,Rudd,90050,9%,03/2013").build();
        Assert.assertEquals(21265.5, taxChart.getTaxRate(em), 0.001);
    }


    @Test
    public void testGetTaxRate_Over180000() throws Exception {

        Employee em = new Employee.EmployeeBuilder().buildEmployeeWithLine("David,Rudd,190050,9%,03/2013").build();
        Assert.assertEquals(59069.5, taxChart.getTaxRate(em), 0.001);
    }

}