package au.com.test.inputs;

import au.com.test.AppBulk;
import au.com.test.domain.Employee;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class EmployeesFileLoaderTest {
    @Test
    public void execute_ShouldLoadTheDefaultFile() throws Exception {
        EmployeesFileLoader executor = new EmployeesFileLoader();
        String path = executor.generateFilePath(new String[0]);
        Assert.assertNotNull( path);
    }

    @Test
    public void execute_ShouldLoadTheDefaultFile_WhenArgsisNull() throws Exception {
        EmployeesFileLoader executor = new EmployeesFileLoader();
        String path = executor.generateFilePath(null);
        Assert.assertNotNull( path);
    }

    @Test
    public void execute_ShouldLoadTheSpecificFile() throws Exception {
        EmployeesFileLoader executor = new EmployeesFileLoader( );
        String datafileLocation = AppBulk.class.getClassLoader().getResource(".").getPath() + "/data.csv";
        List<Employee> employeeList = executor.readEmployeeFromFile(datafileLocation);
        Assert.assertEquals(2, employeeList.size());
    }

}