package au.com.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;

public class SalaryServiceTest {

    @Test
    public void testSalaryServiceExpectSalaryServiceInstance() throws Exception {
        SalaryService result = new SalaryService();
        assertNotNull(result);
    }

    @Test
    public void testCalculateSalaryExpectEmptyResult() throws Exception {
        SalaryService fixture = new SalaryService();

        List<Salary> result = fixture.calculateSalaries(new ArrayList<>());

        // add additional test code here
        assertNotNull(result);
        assertEquals(0, result.size());
    }

}