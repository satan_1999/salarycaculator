package au.com.test.service;

import au.com.test.domain.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-applicationContext.xml")
public class TaxChartHelperIntegrationTest {
    @Autowired
    private TaxChartHelper taxChartHelper;

    @Test
    public void testGetTaxRate_ShouldSupport2012To2018() throws Exception {

        Employee em = new Employee.EmployeeBuilder().buildEmployeeWithLine("David,Rudd,60050,9%,03/2013").build();
        assertNotNull(taxChartHelper.getTaxAmount(2012, em));
        assertNotNull(taxChartHelper.getTaxAmount(2013, em));
        assertNotNull(taxChartHelper.getTaxAmount(2014, em));
        assertNotNull(taxChartHelper.getTaxAmount(2015, em));
        assertNotNull(taxChartHelper.getTaxAmount(2016, em));
        assertNotNull(taxChartHelper.getTaxAmount(2017, em));
        assertNotNull(taxChartHelper.getTaxAmount(2018, em));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTaxRate_ShouldNotSupport2011() throws Exception {

        Employee em = new Employee.EmployeeBuilder().buildEmployeeWithLine("David,Rudd,60050,9%,03/2011").build();
        taxChartHelper.getTaxAmount(2011, em);
    }


}