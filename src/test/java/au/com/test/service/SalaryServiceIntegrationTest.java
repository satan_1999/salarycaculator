package au.com.test.service;

import au.com.test.domain.Employee;
import au.com.test.domain.Salary;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-applicationContext.xml")
public class SalaryServiceIntegrationTest {
    @Autowired
    private SalaryService salaryService;

    @Test
    public void testCalculateSalary_ShouldBeOk() throws Exception {
        Salary salary = salaryService.calculateSalary(new Employee.EmployeeBuilder()
                .buildEmployeeWithLine("Ryan,Chen,120000,10%,03/2013").build());

        assertEquals(10000, salary.getGrossIncome().intValue());
        assertEquals(2696, salary.getIncomeTax().intValue());
        assertEquals(7304, salary.getNetIncome().intValue());
        assertEquals(1000, salary.getSuperannuation().intValue());
    }

}