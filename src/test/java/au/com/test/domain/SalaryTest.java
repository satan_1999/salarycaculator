package au.com.test.domain;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class SalaryTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private Employee employee;

    /**
     * Perform pre-test initialization.
     */
    @Before
    public void setUp() throws Exception {
        employee = new Employee.EmployeeBuilder().buildEmployeeWithLine("firstname,lastname,60050,9%,03/2017").build();
    }

    /**
     * Perform post-test clean-up.
     */
    @After
    public void tearDown() throws Exception {
        // Add additional tear down code here
    }


    /**
     * Run the int getGrossIncome() method test.
     */
    @Test
    public void testGetGrossIncomeExpect5004() throws Exception {
        Salary fixture = new Salary.SalaryBuilder().setGrossIncome(5004.01).build();
        int result = fixture.getGrossIncome().intValue();

        // add additional test code here
        assertEquals(5004, result);
    }

    /**
     * Run the int getIncomeTax() method test.
     */
    @Test
    public void testGetIncomeTaxShouldReturn10() throws Exception {
        Salary fixture = new Salary.SalaryBuilder().setIncomeTax(10.01).build();
        assertEquals(10, fixture.getIncomeTax().intValue());
    }

    /**
     * Run the String getName() method test.
     */
    @Test
    public void testGetNameExpectfirstnamelastname() throws Exception {
        Salary fixture = new Salary.SalaryBuilder().setName("firstname lastname").build();
        String result = fixture.getName();

        assertEquals("firstname lastname", result);
    }

    /**
     * Run the int getNetIncome() method test.
     */
	@Test
	public void testGetNetIncomeExpectReturn4082() throws Exception {
        Salary fixture = new Salary.SalaryBuilder().setGrossIncome(5082).setIncomeTax(1000).build();
		BigDecimal result = fixture.getNetIncome();

		// add additional test code here
		assertEquals(4082, result.intValue());
	}

    /**
     * Run the int getSuperannuation() method test.
     */
    @Test
    public void testGetSuperannuationExpect450() throws Exception {
        Salary fixture = new Salary.SalaryBuilder().setSuperannuation(450).build();

        BigDecimal result = fixture.getSuperannuation();

        // add additional test code here
        assertEquals(450, result.intValue());
    }

    /**
     * Run the void setGrossIncome(double) method test.
     */
    @Test
    public void testSetGrossIncomeExpect10() throws Exception {
        Salary fixture = new Salary.SalaryBuilder().setGrossIncome(10.02).build();
        assertEquals(10 , fixture.getGrossIncome().intValue());
    }

    @Test
    public void testSetGrossIncomeExpect11() throws Exception {
        Salary fixture = new Salary.SalaryBuilder().setGrossIncome(10.52).build();
        assertEquals(11 , fixture.getGrossIncome().intValue());
    }

    /**
     * Run the void setIncomeTax(int) method test.
     */
    @Test
    public void testSetIncomeTaxExpect10() throws Exception {
        Salary fixture = new Salary.SalaryBuilder().setIncomeTax(10.02).build();
        assertEquals(10 , fixture.getIncomeTax().intValue());
    }

    @Test
    public void testSetIncomeTaxExpect11() throws Exception {
        Salary fixture = new Salary.SalaryBuilder().setIncomeTax(10.62).build();
        assertEquals(11 , fixture.getIncomeTax().intValue());
    }

    /**
     * Run the void setSuperannuation(double) method test.
     */
    @Test
    public void testSetSuperannuationExpect10() throws Exception {
        Salary fixture = new Salary.SalaryBuilder().setSuperannuation(10.22).build();
        assertEquals(10 , fixture.getSuperannuation().intValue());
    }

    @Test
    public void testSetSuperannuationExpect11() throws Exception {
        Salary fixture = new Salary.SalaryBuilder().setSuperannuation(10.52).build();

        assertEquals(11 , fixture.getSuperannuation().intValue());
    }

    /**
     * Run the String toString() method test.
     */
    @Test
    public void testToStringExpectString() throws Exception {
        Salary fixture = new Salary.SalaryBuilder().setGrossIncome(5004.0).setName("firstname lastname").setPayPeriod("01 Feb - 28 Feb")
    	.setIncomeTax(922.0).setSuperannuation(450.0).build();

        String result = fixture.toString();

        // add additional test code here
        assertEquals("firstname lastname,01 Feb - 28 Feb,5004,922,4082,450", result);
    }

}