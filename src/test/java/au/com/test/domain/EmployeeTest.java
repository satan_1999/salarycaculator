package au.com.test.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.joda.time.LocalDate;
import org.junit.Test;

public class EmployeeTest {
    /**
     * Run the Employee() constructor test.
     */
    @Test
    public void testEmployeeExpectCreateInstance() {
        Employee result = new Employee();
        assertNotNull(result);
    }

    /**
     * Run the int getAnnualSalary() method test.
     */
    @Test
    public void testSetGetAnnualSalaryExpect1() {
        Employee fixture = new Employee.EmployeeBuilder().setAnnualSalary(1).build();
        BigDecimal result = fixture.getAnnualSalary();

        assertEquals(1, result.intValue());
    }

    /**
     * Run the String getFirstName() method test.
     */
    @Test
    public void testSetGetFirstNameExpectTom() {
        Employee fixture = new Employee.EmployeeBuilder().setFirstName("Tom").build();
        assertEquals("Tom", fixture.getFirstName());
    }

    /**
     * Run the String getLastName() method test.
     */
    @Test
    public void testSetGetLastNameExpectTom() throws Exception {
        Employee fixture = new Employee.EmployeeBuilder().setLastName("Tom").build();
        assertEquals("Tom", fixture.getLastName());
    }

    /**
     * Run the String getPaymentPeriod() method test.
     */
    @Test
    public void testGetPaymentStartDateExpectCorrectDateRange() throws Exception {
        Employee fixture = new Employee.EmployeeBuilder().setPayStart(new LocalDate(2015, 3, 1).toDate()).setPayEnd(new LocalDate(2015, 3, 31).toDate()).build();
        assertEquals("01 March 2015 - 31 March 2015", fixture.getPaymentPeriod());
    }

    /**
     * Run the double getSuperRate() method test.
     */
    @Test
    public void testSetGetSuperRateExpect0dot11() throws Exception {
        Employee fixture = new Employee.EmployeeBuilder().setSuperRate("11%").build();
        assertEquals(0.11, fixture.getSuperRate(), 4);
    }

    /**
     * Run the void setEmployee(String) method test.
     */
    @Test(expected = java.lang.IllegalArgumentException.class)
    public void testSetEmployeeExpectIllegalArgumentException2() throws Exception {
        new Employee.EmployeeBuilder().buildEmployeeWithLine("a").build();
    }

    /**
     * Run the void setEmployee(String) method test.
     */
    @Test
    public void testSetEmployeeExpectNoException() throws Exception {
        String line = "Ryan,Chen,120000,10%,03/2017";
        Employee fixture = new Employee.EmployeeBuilder().buildEmployeeWithLine(line).build();
        assertEquals(120000, fixture.getAnnualSalary().intValue());
        assertEquals("Ryan", fixture.getFirstName());
        assertEquals("Chen", fixture.getLastName());
        assertEquals(0.1, fixture.getSuperRate(), 4);
        assertEquals("01 March 2017 - 31 March 2017", fixture.getPaymentPeriod());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetEmployeeExpectIllegalArgumentException1() {
        String line = "Ryan,Chen,1A0000,10%,03/2014";
        new Employee.EmployeeBuilder().buildEmployeeWithLine(line).build();
    }

    /**
     * Run the void setEmployee(String) method test.
     */
    @Test(expected = java.lang.IllegalArgumentException.class)
    public void testSetEmployeeExpectIllegalArgumentException() {
        String line = "";
        new Employee.EmployeeBuilder().buildEmployeeWithLine(line).build();
    }

    /**
     * Run the void setSuperRate(String) method test.
     */
    @Test
    public void testSetSuperRateExpectEmptyResult() {
        Employee fixture = new Employee.EmployeeBuilder().setSuperRate("11").build();
        assertEquals(0.0, fixture.getSuperRate(), 4);
    }

    /**
     * Run the void setSuperRate(String) method test.
     */
    @Test(expected = NumberFormatException.class)
    public void testSetSuperRateExpectNumberFormatException() {
        new Employee.EmployeeBuilder().setSuperRate("A%").build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetSuperRateOver50ShouldThrowException() {
        new Employee.EmployeeBuilder().setSuperRate("58%").build();
    }


}