# Employee monthly payslip

### Author : Ke (Wallace) Tan

Description: 
===

When I input the employee's	details:	first	name,	last	name,	annual	salary(positive	integer)	and	super	
rate(0%	- 50%	inclusive),	payment	start	date,	the	program	should	generate	payslip	information	with	
name,	pay	period,	 gross	income,	income	au.com.test.tax,	net	income	and	super.

The	calculation	details	will	be	the	following:

•	 pay	period	=	per	calendar	month
•	 gross	income	=	annual	salary	/	12	months
•	 income	au.com.test.tax	=	based	on	the	au.com.test.tax	table	provide	below
•	 net	income	=	gross	income	- income	au.com.test.tax
•	 super	=	gross	income	x	super	rate

Notes:	All	calculation	results	should	be	rounded	to	the	whole	dollar.	If	>=	50	cents	round	up	to	the	next	
dollar	increment,	otherwise	round	down.

The	following	rates	for	2012-13	apply	from	1	July	2012.

Taxable	income	 Tax	on	this	income

* 0	- $18,200	 Nil
* $18,201	- $37,000	 19c	for	each	$1	over	$18,200
* $37,001	- $80,000	 $3,572	plus	32.5c	for	each	$1	over	$37,000
* $80,001	- $180,000	 $17,547	plus	37c	for	each	$1	over	$80,000
* $180,001	and	over	 $54,547	plus	45c	for	each	$1	over	$180,000

Example	Data

Employee	annual	salary	is	60,050,	super	rate	is	9%,	how	much	will	this	employee	be	paid	for	the	month	
of	March	?
*  pay	period	=	Month	of	March	(01	March	to	31	March)
*  gross	income	=	60,050	/	12	=	5,004.16666667	(round	down)	=	5,004
*  income	au.com.test.tax	=	(3,572	+	(60,050	- 37,000)	x	0.325)	/	12	 =	921.9375	(round	up)	=	922
*  net	income	=	5,004	- 922	=	4,082
*  super	=	5,004	x	9%	=	450.36	(round	down)	=	450

Here	is	the	csv	input	and	output	format	we	provide.	(But	feel	free	to	use	any	format	you	want)
Input	(first	name,	last	name,	annual	salary,	super	rate	(%),	payment	start	date):
* David,Rudd,60050,9%,01	March	– 31	March
* Ryan,Chen,120000,10%,01	March	– 31	March

Output	(name,	pay	period,	gross	income,	income	au.com.test.tax,	net	income,	super):
* David	Rudd,01	March	– 31	March,5004,922,4082,450
* Ryan	Chen,01	March	– 31	March,10000,2696,7304,1000

As	part	of	your	solution:
* List	any	assumptions	that	you	have	made	in	order	to	solve	this	problem.
* Provide	instruction	on	how	to run	the	application
* We	value	good	design	and	test,	it’s	important	to	us


Hypothesis 
====
* The Leap year is not considered.
* Any invalid input will occur exception.
* The input will be a File or Console.
* The calculator only calculate the regular monthly salary.
* The sample of Input format is _Jack,Rudd,60050,15%,06/2014_

Design/Architecture 
====
* The application uses service/helper patten with Spring Context. All beans would be bootstrapped by Spring.
* Use builder pattern ( _SalaryBuilder_, _EmployeeBuilder_)to convert the raw input into required domain objects.
* The Design will follow Test Driven Design.
* Use Chain - Design Pattern (_SalaryRootHandler_) to calculate the salary/payslip.
* The user can specify the data file via args in command.
* For simplicity, the TaxChart is registered into application with year. However, it would be better to store them into DB in the commercial application.
* If the pay year is earlier than 2012, throw out exception.
     Example :  
     
       There is no taxChart for Year - 2011


System Requirements 
====
* Java 8 is required.
* Maven 3.3.1+ is required.
* This project is a simple maven project.

How to run it 
====
To build the application and run all tests, please execute the following command in the root directory

      mvn clean install

##### File Input: 
To run the application with the default data file, please execute the following command in the root directory
      
      mvn exec:java@fileInput
   
To run the application with the specific data file, please execute the following command in the root directory
 
      mvn exec:java@fileInput -Dexec.args="<absolutePathOfFile>"

This sample of output is below:

        David Rudd,01 March 2014 - 31 March 2014,5004,922,4082,450
        David Rudd,01 February 2014 - 28 February 2014,5004,922,4082,450
        David Rudd,01 June 2013 - 30 June 2013,5004,922,4082,450
        Ryan Chen,01 March 2014 - 31 March 2014,10000,2696,7304,1000
        Tom Rudd,01 March 2014 - 31 March 2014,5004,922,4082,450
        Jerry Rudd,01 June 2014 - 30 June 2014,5004,922,4082,450
        Jack Rudd,01 June 2014 - 30 June 2014,5004,922,4082,751
        Tim Rudd,01 June 2012 - 30 June 2012,5004,922,4082,751



##### Console Input:
 
 To run the application with console


    mvn exec:java@consoleInput
    
 Once the console is started, you can see the following content in the screen.
     
    
    +++++++++++++++++++++++++++++++++++++++++++++++
    ++++++++++++++ Salary Calculator ++++++++++++++
    +++++++++++++++++++++++++++++++++++++++++++++++
    Please Type in Pay Details in comma delimited form(FirstName,LastName,AnnualSalary,SuperPercentage,salaryMonthAndYear) to get Pay Slip or Type EXIT to close application.

    
 And then, you can put the entry like 
  
      David,Rudd,60050,9%,02/2014
   
    
This output is below:

      David Rudd,01 February 2014 - 28 February 2014,5004,922,4082,450
    
Auto-Test
====
There are 45 test cases. 

 To run all test cases


    mvn test
    